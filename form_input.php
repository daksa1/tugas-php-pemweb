<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <title>Form Penilaian</title>
    <style>
body{
    background: -webkit-linear-gradient(left, #0072ff, #00c6ff);
}
.form{
    background: #fff;
    margin-top: 10%;
    margin-bottom: 5%;
    width: 50%;
}
.form .form-control{
    border-radius:1rem;
}
.form form{
    padding: 14%;
}
.form form .row{
    margin-bottom: -7%;
}
.form h3{
    margin-bottom: 8%;
    margin-top: -10%;
    text-align: center;
    color: #0062cc;
}
.btnGenerate {
    width: 50%;
    border: none;
    border-radius: 1rem;
    padding: 1.5%;
    background: #dc3545;
    font-weight: 600;
    color: #fff;
    cursor: pointer;
}
.btnGenerate:hover{
    width: 50%;
    border-radius: 1rem;
    padding: 1.5%;
    color: #fff;
    background-color: #8f1010;
    border: none;
    cursor: pointer;
}
</style>
</head>
<body>
<div class="container form">
            <form action="form_output.php" method="post">
                <h3>Form Penialain Siswa/Siswi</h3>
               <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <input type="text" name="name" class="form-control" placeholder="Nama Siswa/Siswi *" id="name"required/>
                        </div>
                        <div class="form-group">
                            <input type="text" name="mapel" class="form-control" placeholder="Mata Pelajaran *" required/>
                        </div>
                        <div class="form-group">
                            <input type="number" min="0" max="100" name="nilaiuts" class="form-control" placeholder="Nilai UTS *" />
                        </div>
                        <div class="form-group">
                            <input type="number" min="0" max="100" name="nilaiuas" class="form-control" placeholder="Nilai UAS *" />
                        </div>
                        <div class="form-group">
                            <input type="number" min="0" max="100" name="nilaitugas" class="form-control" placeholder="Nilai Tugas *" />
                        </div>
                        <div class="text-center">
                            <input type="submit" name="submit" class="btnGenerate" value="Hitung">
                        </div>
                    </div>
                </div>
            </form>
</div>
</body>
</html>

