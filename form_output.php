<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" 
    integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">

    <title>Rekapan Nilai</title>
  </head>
  <body>
  <?php
        $name = $_POST['name'];
        $mapel = $_POST['mapel'];
        $nilaiuas = $_POST['nilaiuas'];
        $nilaiuts = $_POST['nilaiuts'];
        $nilaitugas = $_POST['nilaitugas'];
        $nilaiakhir = ($nilaiuas * 0.50)+($nilaiuts * 0.35)+($nilaitugas * 0.15);
        if ($nilaiakhir >= 90 && $nilaiakhir <= 100) {
          $grade="A";
        }
        else if ($nilaiakhir > 70 && $nilaiakhir < 90) {
            $grade="B";
          }
        else if ($nilaiakhir > 50 && $nilaiakhir <= 70) {
            $grade="C";
          }
        else if ($nilaiakhir <= 50) {
            $grade="D";
          }
    ?>
    <center><h1>Rekapan Nilai</h1></center>
    <table class="table table-dark table-striped">
  <thead>
    <tr>
      <th>Nama</th>
      <th>Mata Pelajaran</th>
      <th>Nilai UTS</th>
      <th>Nilai UAS</th>
      <th>Nilai Tugas</th>
      <th>Nilai Akhir</th>
      <th>Grade</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>
                <?php echo $name ?>
        </td>
        <td>
                <?php echo $mapel ?>
        </td>
        <td>
                <?php echo $nilaiuts ?>
        </td>
        <td>
                <?php echo $nilaiuas ?>
        </td>
        <td>
                <?php echo $nilaitugas ?>
        </td>
        <td>
                <?php echo $nilaiakhir ?>
        </td>
        <td>
                <?php echo $grade ?>
        </td>
    </tr>
  </tbody>
</table>  
  </body>
</html>